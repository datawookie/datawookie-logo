#!/bin/bash

# Create smaller PNG images.
#
convert face-512.png -resize 192x192 face-192.png
convert face-512.png -resize 64x64 face-64.png
convert face-512.png -resize 32x32 face-32.png

# Create JPG images.
#
convert face-512.png face-512.jpg
convert face-512.png -resize 192x192 face-192.jpg
convert face-512.png -resize 64x64 face-64.jpg
convert face-512.png -resize 32x32 face-32.jpg
